import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class DataInterpreter {
    private List<MonitoredData> dataList;
    private String filename;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public DataInterpreter(String filename) {
        this.filename = filename;
        dataList = new ArrayList<>();
    }

    public void loadData(boolean printDuration) {
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get(filename));

            if (stream == null)
                System.exit(-1);

            stream.forEach(line -> {
                ParsePosition pp = new ParsePosition(0);
                Date start = dateFormat.parse(line, pp);
                Date end = dateFormat.parse(line, pp);
                String activity = line.substring(pp.getIndex()).trim();

                MonitoredData d = new MonitoredData(start, end, activity);
                dataList.add(d);

                if (printDuration)
                    System.out.println(d.getActivity() + " " + d.getDuration().getHours() + "h " + d.getDuration().getMinutes() +"m " + d.getDuration().getSeconds() + "s");
            });

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            stream.close();
        }
    }

    public void countDaysOfData() {
        Date total = new Date(0);
        dataList.forEach(monitoredData ->
            total.setTime(total.getTime() + monitoredData.getDuration().getTime())
        );

        System.out.println(Math.round((total.getTime() / (1000*3600*24.0)) * 100.0) / 100.0 + " total days in recorded activity time.");
    }

    public void countPeriodInDays() {
        Date diff = new Date(dataList.get(dataList.size() - 1).getEndTime().getTime() - dataList.get(0).getStartTime().getTime());

        System.out.println("Data has been recorded over a period of " + Math.round(diff.getTime() / (1000*3600*24.0)) + " days.");
    }

    public Map<String, Integer> countEachActivity() {
        Map<String, Integer> out = new HashMap<>();

        dataList.forEach(monitoredData ->
            out.merge(monitoredData.getActivity(), 1, Integer::sum)
        );

       out.keySet().forEach(s ->  {
            System.out.println("'" + s + "'" + " occured " + out.get(s) + " times.");
        });

        return out;
    }

    public void countEachActivityEachDay() {
        class ActivityDayPair  implements Comparable<ActivityDayPair> {
            private String activity;
            private String day;

            public ActivityDayPair(String activity, String day) {
                this.activity = activity;
                this.day = day;
            }

            public String getActivity() {
                return activity;
            }

            public String getDay() {
                return day;
            }

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;
                ActivityDayPair that = (ActivityDayPair) o;
                return Objects.equals(activity, that.activity) &&
                        Objects.equals(day, that.day);
            }

            @Override
            public int hashCode() {
                return Objects.hash(activity, day);
            }

            @Override
            public int compareTo(ActivityDayPair o) {
                int thisDay, thisMonth, oDay, oMonth;
                String[] thisParts = day.split(" ");
                String[] oParts = o.getDay().split(" ");
                thisMonth = Integer.parseInt(thisParts[0]);
                thisDay = Integer.parseInt(thisParts[1]);

                oMonth = Integer.parseInt(oParts[0]);
                oDay = Integer.parseInt(oParts[1]);

                if (thisMonth == oMonth) {
                    return thisDay - oDay;
                } else {
                    return thisMonth - oMonth;
                }
            }
        }

        Map<ActivityDayPair, Integer> map = new HashMap<>();
        SimpleDateFormat df = new SimpleDateFormat("MM dd");
        dataList.forEach(monitoredData -> {
            ActivityDayPair adp = new ActivityDayPair(monitoredData.getActivity(),
                                                        df.format(monitoredData.getStartTime()));

            map.merge(adp, 1, Integer::sum);
        });

        List<ActivityDayPair> keySet = new ArrayList<>(map.keySet());
        Collections.sort(keySet);

        keySet.forEach(activityDayPair ->
            System.out.println(activityDayPair.getDay() + " , activity '" +
                    activityDayPair.getActivity() + "' happened " +
                    map.get(activityDayPair) + " times.")
        );
    }

    public void totalDurationEachActivity() {
        Map<String, Long> out = new HashMap<>();

        dataList.forEach(monitoredData ->
                out.merge(monitoredData.getActivity(), monitoredData.getDuration().getTime(), Long::sum)
        );

        out.keySet().forEach(s -> {
            Date duration = new Date(out.get(s));
            System.out.println("'" + s + "'" + " happened for a total of " + duration.getDay() + "d " + duration.getHours() + "h " + duration.getMinutes() +"m " + duration.getSeconds() + "s.");
        });
    }

    public void filterShortActivities() {
        class ActivityDurationHelper {
            String activity;
            int lessThan5MinCount;

            ActivityDurationHelper(String activity) {
                this.activity = activity;
                lessThan5MinCount=0;
            }
        }

        Map<String, List<MonitoredData>> map = new HashMap<>();

        dataList.forEach(monitoredData -> {
            if (map.containsKey(monitoredData.getActivity())) {
                map.get(monitoredData.getActivity()).add(monitoredData);
            } else {
                map.put(monitoredData.getActivity(), new ArrayList<>());
            }
        });

        Map<String, ActivityDurationHelper> map2 = new HashMap<>();
        map.keySet().forEach(s -> map2.put(s, new ActivityDurationHelper(s)));

        dataList.forEach(monitoredData -> {
                if (monitoredData.getDuration().getTime() < 5*60*1000 ) { // 5 minutes * 60 sec * 1000 milisec
                    map2.get(monitoredData.getActivity()).lessThan5MinCount++;
                }
        });

        dataList.stream().filter(monitoredData ->
                1.0 * map2.get(monitoredData.getActivity()).lessThan5MinCount / map.get(monitoredData.getActivity()).size() <= 0.9
        ).forEach(System.out::println);
    }
}
