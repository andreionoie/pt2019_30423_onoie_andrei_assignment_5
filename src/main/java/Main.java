public class Main {
    public static void main(String[] args) {
        DataInterpreter dataInterpreter = new DataInterpreter("Activities.txt");

        dataInterpreter.loadData(false);
        dataInterpreter.countPeriodInDays();
        dataInterpreter.countDaysOfData();
        dataInterpreter.countEachActivity();
        dataInterpreter.countEachActivityEachDay();
        dataInterpreter.totalDurationEachActivity();
        dataInterpreter.filterShortActivities();
    }
}
