import java.text.SimpleDateFormat;
import java.util.Date;

public class MonitoredData {
    private final Date startTime;
    private final Date endTime;
    private final String activity;

    public MonitoredData(Date startTime, Date endTime, String activity) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public Date getStartTime() {
        return startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public Date getDuration() {
        return new Date(endTime.getTime() - startTime.getTime());
    }

    public String getActivity() {
        return activity;
    }

    public String toString() {
        return startTime.toString() + "\t" + endTime.toString() + "\t" + activity;
    }
}
